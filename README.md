**This is a temporary repo for testing.**

Original repo is acheived at https://github.com/permamodel/permamodel

**Overeem, I., E. Jafarov, K. Wang, K. Schaefer, S. Stewart, G. Clow, M. Piper, and Y. Elshorbany (2018), A modeling toolbox for permafrost landscapes, Eos, 99, https://doi.org/10.1029/2018EO105155. Published on 28 September 2018.**

Ku components were developed by **Kang Wang**.

- Ku_component with bmi was developed for WMT usage. A global soil database and thermal parameterization approaches were built in.

- KuFlex component with bmi was developed for more flexible use. It exposed more variables in bmi and removed soil thermal parameterization. 
Thus, users need provide these parameters explicitly. 
This accepts 2D+TS+Scalar, 2D+Scalar, 2D+TS, TS+Scalar, and Scalar inputs combination. [TS: time-series, 2D: spatial; Scalar: single value] 

# Installation:

python setup.py develop

# Jupter Notebook Demo: notebooks/KuFlex.ipynb

In this demo, three different cases are:

* to use grid, scalar, and time-series inputs

* to use time-series and scalar inputs

* to use scalar inputs only.